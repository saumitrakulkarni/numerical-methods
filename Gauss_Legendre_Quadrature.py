# -*- coding: utf-8 -*-
"""
@author: Saumitra Kulkarni
"""
import numpy as np

class Gauss_Legendre_Quadrature():
    def __init__(self, xi, xf, polyorder):
        # Intergration interval
        self.xi = xi
        self.xf = xf
        # Order of Legendre polynomial to be used
        self.polyorder = polyorder

    # Recursive generation of the Legendre polynomial of order n
    @staticmethod
    def Legendre(self, polyorder, x):
        if polyorder == 0:
            return x*0+1
        elif polyorder == 1:
            return x
        else:
            return ((2*polyorder-1)*x*self.Legendre(self,polyorder-1, x) - (polyorder-1)*self.Legendre(self,polyorder-2, x)) / polyorder

    # Derivative of the Legendre polynomials
    def DLegendre(self, x):
        if self.polyorder == 0:
            return x*0
        elif self.polyorder == 1:
            return x*0+1
        else:
            return (self.polyorder/(x**2-1)) * (x*self.Legendre(self,self.polyorder, x) - self.Legendre(self,self.polyorder-1,x))

    # Roots of the polynomial obtained using Newton-Raphson method
    def LegendreRoots(self, tolerance = 1e-20):
        if self.polyorder < 2:
            err = 1 # bad polyorder no roots can be found
        else:
            roots = list()
            # The polynomials are alternately even and odd functions. So we evaluate only half the number of roots. 
            for i in range(1, int(self.polyorder/2 +1)):
                x = np.cos(np.pi * (i - 0.25)/(self.polyorder + 0.5))
                error = 10 * tolerance
                itr = 0
                while error > tolerance and itr < 2000:
                    dx = (- self.Legendre(self,self.polyorder,x)/self.DLegendre(x))
                    x += dx
                    itr += 1
                    error = abs(dx)
                roots.append(x)
            # Use symmetry to get the other roots
            roots = np.array(roots)
            if self.polyorder%2 == 0:
                roots = np.concatenate((-1*roots, roots[::-1]))
            else:
                roots = np.concatenate((-1*roots, [0], roots[::-1]))
            err = 0 # successfully determined roots
        return roots, err

    # Weight coefficients
    def GaussLegendreWeights(self):
        W = list()
        xs, err = self.LegendreRoots()
        if err == 0:
            W = 2/((1 - xs**2) * (self.DLegendre(xs)**2))
            err = 0
        else:
            err = 1 # could not determine roots - so no weights
        return W, xs, err

    def compute_integral(self, f, print_values = False):
        Ws, xs, err = self.GaussLegendreWeights()
        if err == 0:
            I = (self.xf - self.xi) * 0.5 * sum(Ws * f((self.xf - self.xi) * 0.5 * xs + (self.xf + self.xi) * 0.5))
            print("Integral : ", I)
            if print_values:
                print("Order    : ", self.polyorder)
                print("Roots    : ", xs)
                print("Weights  : ", Ws)
        else:
            err = 1
            ans = None
            print("Roots/Weights evaluation failed")
            print("Integral evaluation failed")
        return I, err

# The integral value 
# func 		: the integrand
# a, b 		: lower and upper limits of the integral
# polyorder 	: order of the Legendre polynomial to be used

if __name__ == '__main__':
        
    def f1(x):
        return np.sin(x)

    GLQ = Gauss_Legendre_Quadrature(xi = 0, xf = np.pi, polyorder = 5)
    GLQ.compute_integral(f1, print_values = True)
