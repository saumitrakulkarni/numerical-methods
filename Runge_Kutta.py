# -*- coding: utf-8 -*-
"""
@author: Saumitra Kulkarni
"""
import numpy as np
import matplotlib.pyplot as plt
import time

class Runge_Kutta():
    def __init__(self, xi, xf, h, x_init, y_init):
        """
        Calculate the solution of a Ordinary Differential Equation using
        Runge Kutta Method

        Parameters:

        xi: lower limit of the integral
        xf: upper limit of the definite integral (real)
        h: Step size
        x_init: Initial value of x
        y_init: Initial value of y
        
        """
        # Intergration interval
        self.xi = xi
        self.xf = xf
        # Step size
        self.h = h
        # Initial condition
        self.x_init = x_init
        self.y_init = y_init
        # Interval spacing
        self.n = int((self.xf - self.xi) / self.h)

    def initialize(self):
        """
        Initialize the x and y vectors with initial values
        """
        x = np.zeros(self.n)
        x[0] = self.x_init
        y = np.zeros(self.n)
        y[0] = self.y_init

        return y, x

    def compute_method(self, f, scheme: str, save_fig = False, print_values = False):
        """
        Compute the numerical integral

        Parameters:

        f: function or the integrand
        scheme: method of integration; third or fourth order Runge Kutta methods
        save_fig: Boolean variable to save the plot
        print_values: Boolean variable to print integral vales
        
        """
        # Check for appropriate function arguements
        assert(scheme == 'third' or scheme == 'fourth'), "Only third and fourth order schemes are allowed"

        y, x = self.initialize()

        for i in range(self.n-1):
            # Step size increament in x for next y estimate
            x[i+1] = x[i] + self.h
            if scheme == 'third':
                # Calculate derivative approximations
                k1 = f(x[i], y[i])
                k2 = f(x[i] + self.h/2, y[i] + k1 * self.h/2)
                k3 = f(x[i] + self.h/2, y[i] - k1 * self.h + 2 * k2 * self.h)
                # Calculate new y estimate
                y[i+1] = y[i] + 1/6 * (k1 + 4 * k2 + k3) * self.h
            elif scheme == 'fourth':
                # Calculate derivative approximations
                k1 = f(x[i], y[i])
                k2 = f(x[i] + self.h/2, y[i] + k1 * self.h/2)
                k3 = f(x[i] + self.h/2, y[i] + k2 * self.h/2)
                k4 = f(x[i] + self.h, y[i] + k3 * self.h)
                # Calculate new y estimate
                y[i+1] = y[i] + 1/6 * (k1 + 2 * k2 + 2 * k3 + k4) * self.h               
            
            if print_values: print(x[i], y[i])

        # Plot the ODE
        plt.plot(x, y, marker='o')
        plt.grid()
        plt.title(f'{scheme.capitalize()} Order Runge-Kutta Method', fontsize=14)
        plt.xlabel('x')
        plt.ylabel('y')
        if save_fig: plt.savefig(f'{scheme.capitalize()}Order_RK_{time.time()}.png')
        plt.show()

        return x, y

# Driver code
if __name__ == '__main__':

    # ODE example 1
    def f1(x,y):
        return np.sin(x)**2 * y

    print("Runge-Kutta Methods - dy/dx = (sin(x))^2 * y")
    print("0 <= x <= 5; x0 = 0; y0 = 2; h = 0.5")
    print("--------------------------------------")
    em1 = Runge_Kutta(xi = 0, xf = 5, h = 0.5, x_init = 0, y_init = 2)
    print("Third Order Scheme Figure Saved!")
    em1.compute_method(f1, save_fig = True, scheme = 'third')
    print("Fourth Order Scheme Figure Saved!")
    em1.compute_method(f1, save_fig = True, scheme = 'fourth')
