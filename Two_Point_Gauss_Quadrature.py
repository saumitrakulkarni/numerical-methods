# -*- coding: utf-8 -*-
"""
@author: Saumitra Kulkarni
"""

import numpy as np

class Two_Point_Gauss_Quadrature():
    def __init__(self, xi, xf, n):
        """
        Calculate the numerical integral of a function f(x) using
        the two-point Gauss Quadrature rule
        
        Parameters:

        xi: lower limit of the integral
        xf: upper limit of the definite integral (real)
        n: Number of intervals of the function for the integral
        
        """
        # Intergration interval
        self.xi = xi
        self.xf = xf
        # Number of Intervals
        self.n = n
        # Step size
        self.h = (xf - xi)/(self.n)

    def compute_integral(self, f):
        """
        Calculates the numerical integral of a function f(x)
        using the two point Gauss Quadrature rule in interval [xi, xf].
        """

        S1 = sum(f(np.arange(self.xi, self.xf, 0.2113*self.h)))
        S2 = sum(f(np.arange(self.xi, self.xf, 0.7887*self.h)))


        I = (self.h/6) * (S1 + S2)
        
        return I

# Driver code
if __name__ == '__main__':

    # Integrand
    def f1(x):
        return np.exp(x)

    sr = Two_Point_Gauss_Quadrature(xi = -3, xf = 3, n = 100)
    print("Gauss Quadrature Method : y = sin(x)")
    print(f"0 <= x <= pi; n = {sr.n}")
    print("--------------------------------------")
    print("Integral:",sr.compute_integral(f1))
