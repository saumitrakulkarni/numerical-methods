# -*- coding: utf-8 -*-
"""
@author: Saumitra Kulkarni
"""

import numpy as np
import matplotlib.pyplot as plt
import time

class Eulers_Method():
    def __init__(self, xi, xf, n, y_init):
        """
        Calculate the solution of a Ordinary Differential Equation using
        Forward & Backward Euler method

        Parameters:

        xi: lower limit of the integral
        xf: upper limit of the definite integral (real)
        n: Number of intervals of the function for the integral
        y_init: Initial value of y
        
        """
        # Interval end point
        self.xi = xi
        self.xf = xf
        # Number of intervals
        self.n = n
        # Intial condition
        self.y_init = y_init
        # Step size
        self.h = 1
        #(self.xf - self.xi)/self.n

    def initialize(self):
        """
        Initialize the x and y vectors with initial values
        """
        x = np.zeros(self.n)
        x[0] = self.xi
        y = np.zeros(self.n)
        y[0] = self.y_init

        return y, x

    def compute_method(self, f, scheme: str, save_fig = False, print_values = False):
        """
        Compute the numerical integral

        Parameters:

        f: function or the integrand
        scheme: method of integration; forward Euler or backward Euler methods
        save_fig: Boolean variable to save the plot
        print_values: Boolean variable to print integral vales
        
        """
        # Check for appropriate function arguements
        assert(scheme == 'forward' or scheme == 'backward'), "Only forward and backward schemes are allowed"

        # Intialize values
        y, x = self.initialize()
        
        for i in range(self.n-1):
            x[i+1] = x[i] + self.h

            # Forward Euler method
            if scheme == 'forward':
                y[i+1] = y[i] + self.h*f(x[i], y[i])

            # Backward Euler method
            elif scheme == 'backward':
                yprime = y[i] + self.h*f(x[i], y[i])
                y[i+1] = y[i] + self.h*f(x[i+1], yprime)

            if print_values: print(x[i], y[i])

        # Plot the ODE
        plt.plot(x, y, marker='o')
        plt.grid()
        plt.title(f'{scheme.capitalize()} Euler Method', fontsize=14)
        plt.xlabel('x')
        plt.ylabel('y')
        if save_fig: plt.savefig(f'{scheme.capitalize()}Euler_{time.time()}.png')
        plt.show()

        return y, x

# Driver code
if __name__ == '__main__':

    # ODE example 1
    def f1(x,y):
        return 0*y + np.exp(x)

    # ODE example 2
    def f2(x,y):
        return - y

##    print("Euler's Methods - dy/dx = e^x")
##    print("0 <= x <= 2; y0 = 1")
##    print("--------------------------------------")
##    em1 = Eulers_Method(xi = 0, xf = 2, n = 10, y_init = 1)
##    print("Forward Scheme Figure Saved!")
##    em1.compute_method(f1, save_fig = True, scheme = 'forward', print_values = True)
##    print("Backward Scheme Figure Saved!")
##    em1.compute_method(f1, save_fig = True, scheme = 'backward', print_values = True)
##
##    print("--------------------------------------")
    
    print("Euler's Methods - dy/dt = y - x^2 + 1")
    print("0 <= x <= 2; y0 = 0.5")
    print("--------------------------------------")
    em2 = Eulers_Method(xi = 1, xf = 3, n = 10, y_init = 1)
    print("Forward Scheme Figure Saved!")
    em2.compute_method(f2, save_fig = True, scheme = 'forward', print_values = True)
    print("Backward Scheme Figure Saved!")
    em2.compute_method(f2, save_fig = True, scheme = 'backward', print_values = True)
    
