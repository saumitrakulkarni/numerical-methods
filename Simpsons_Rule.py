# -*- coding: utf-8 -*-
"""
@author: Saumitra Kulkarni
"""

import numpy as np
import matplotlib.pyplot as plt
import time

class Simpsons_Rule():
    def __init__(self, xi, xf, n):
        """
        Calculate the numerical integral of a function f(x) using
        the Simpson's 1/3rd and 3/8th rule and subsequent composite versions of these rules

        Parameters:

        xi: lower limit of the integral
        xf: upper limit of the definite integral (real)
        n: Number of intervals of the function for the integral
        
        """
        # Intergration interval
        self.xi = xi
        self.xf = xf
        # Number of Intervals
        self.n = n
        # Step size
        self.h = (xf - xi)/(self.n)

    def initialize(self):
        """
        Initialize the x vector and value of integral
        """
        x = np.linspace(self.xi, self.xf, self.n+1)
        I = 0
        return x, I

    def compute_integral(self, f, scheme: str, print_values = False):
        """
        Compute the numerical integral

        Parameters:

        f: function or the integrand
        scheme: method of integration; 1/3rd, 3/8th, composite 1/3rd, and composite 3/8th rules
        print_values: Boolean variable to print integral vales
        
        """
        # Check for appropriate function arguements
        if scheme == '1/3rd' and self.n%2 != 0: raise ValueError("For 1/3rd rule, n must be divisible by 2")
        elif scheme == '3/8th' and self.n%3 != 0: raise ValueError("For 3/8th rule, n must be divisible by 3")

        # Intialize values
        x, I = self.initialize()

        # Simpson's 1/3rd rule
        if scheme == '1/3rd':
            I = (self.h/3) * (f(x[0]) + 2*sum(f(x[:self.n-2:2])) + 4*sum(f(x[1:self.n-1:2])) + f(x[-1]))
            
        # Composite simpson's 1/3rd rule
        elif scheme == 'composite_1/3rd':
            for i in range(int((self.n-2)/2) + 1):
                I += self.h/3 * (f(x[2*i]) + 4*f(x[2*i + 1]) + f(x[2*i + 2]))
                if print_values: print(x[i], I)
                
        # Simpson's 3/8th rule
        elif scheme == '3/8th':
            I = (3*self.h/8) * (f(x[0]) + 3*sum(f(x[:self.n-2:3])) + 3*sum(f(x[1:self.n-1:3])) + 2*sum(f(x[2:self.n-3:3])) + f(x[-1]))
            
        # Composite simpson's 3/8th rule
        elif scheme == 'composite_3/8th':
            for i in range(int((self.n-3)/3) + 1):
                I += 3*self.h/8 * (f(x[3*i]) + 3*f(x[3*i + 1]) + 3*f(x[3*i + 2]) + f(x[3*i + 3]))
                if print_values: print(x[i], I)

        return I

# Driver code
if __name__ == '__main__':

    # Integrand
    def f1(x):
        return np.sin(x)

    sr = Simpsons_Rule(xi = 0, xf = np.pi, n = 60)
    print("Simpson's Method : y = sin(x)")
    print(f"0 <= x <= pi; n = {sr.n}")
    print("--------------------------------------")
    print("Integral by 1/3rd Rule:",sr.compute_integral(f1, scheme = '1/3rd'))
    print("--------------------------------------")
    print("Integral by Composite 1/3rd Rule:",sr.compute_integral(f1, scheme = 'composite_1/3rd'))
    print("--------------------------------------")
    print("Integral by 3/8th Rule:",sr.compute_integral(f1, scheme = '3/8th'))
    print("--------------------------------------")
    print("Integral by Composite 3/8th Rule:",sr.compute_integral(f1, scheme = 'composite_3/8th'))
